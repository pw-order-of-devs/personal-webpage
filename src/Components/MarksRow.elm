module Components.MarksRow exposing (..)


import Html exposing (..)
import Html.Attributes exposing (..)

import Components.Common exposing (..)
import Components.Translation exposing (i18n)


marksRow model = div (columnStyle ++ (paddedTailSmall model))
  [ div (marksStyle model)
    [ div [] []
    , div [ style "color" "#51d8dd" ] [ text ((i18n model "marks.project") ++ ": Karolina Zep") ]
    ]
  ]

marksStyle model = marksStyleCommon ++
  if isDesktop model then marksStyleDesktop
  else if (isMobile model) then marksStyleMobile
  else marksStyleMobileSmall
marksStyleCommon = [ style "width" "100%" , style "display" "flex", style "justify-content" "space-between" ]
marksStyleMobileSmall = [ style "padding" "0 20px" ]
marksStyleMobile = [ style "padding" "0 40px" ]
marksStyleDesktop = [ style "padding" "0 100px" ]
