module Components.Translation exposing (i18n)


import Dict exposing (Dict)


i18n model key =
  case Dict.get key (getLangDict model.lang) of
    Nothing ->
      case Dict.get key (getLangDict "common") of
        Nothing -> "###"
        Just translation -> translation
    Just translation -> translation

getLangDict lang =
  case Dict.get lang i18nDict of
     Nothing -> Dict.fromList []
     Just langDict -> langDict

i18nDict : Dict String (Dict String String)
i18nDict =
  Dict.fromList
    [ ("en", Dict.fromList
        [ ("work.company.division", "Healthcare Division")
        , ("work.company.role", "Programmer, Software Architect")
        , ("studies.master", "Master studies at the")
        , ("studies.master.university", "Wrocław University of Technology")
        , ("studies.master.spec", "specialization: ")
        , ("hobby.programming", "Programming")
        , ("hobby.hiking", "Hiking")
        , ("hobby.travels", "Travels")
        , ("marks.project", "Project")
        ]
      )
    , ("pl", Dict.fromList
        [ ("work.company.division", "Pion Ochrony Zdrowia")
        , ("work.company.role", "Programista, Architekt Oprogramowania")
        , ("studies.master", "Studia magisterskie na")
        , ("studies.master.university", "Politechnice Wrocławskiej")
        , ("studies.master.spec", "specjalizacja: ")
        , ("hobby.programming", "Programowanie")
        , ("hobby.hiking", "Turystyka górska")
        , ("hobby.travels", "Podróże")
        , ("marks.project", "Projekt")
        ]
      )
    , ("common", Dict.fromList
        [ ("my.name", "PAWEŁ WALUS")
        , ("my.company.name", "PAWEŁ WALUS - ORDER OF DEVS")
        , ("work.company.name", "Asseco Poland S.A.")
        , ("studies.master.spec.content", "Computer Security")
        , ("certificate.cae.line.1", "Certificate in")
        , ("certificate.cae.line.2", "Advanced English")
        , ("certificate.cae.line.3", "(CAE)")
        , ("hobby", "Hobby:")
        , ("hobby.fantasy", "Fantasy")
        ]
      )
    ]