module Components.ContentRow exposing (contentRow)


import Bootstrap.Grid as Grid
import Bootstrap.Grid.Col as Col
import Bootstrap.Grid.Row as Row

import Html exposing (..)
import Html.Attributes exposing (..)

import Components.Common exposing (..)
import Components.Translation exposing (i18n)


contentRow model = div (columnStyle ++ (contentStyle model)) [ contentRowInternal model ]

contentStyle model = contentStyleCommon ++
  if isDesktop model then contentStyleDesktop
  else if (isMobile model) then contentStyleMobile
  else contentStyleMobileSmall
contentStyleCommon = [ style "background-color" "#51D8DD" ]
contentStyleMobileSmall = [ style "padding" "30px 0" ]
contentStyleMobile = [ style "padding" "40px 0" ]
contentStyleDesktop = [ style "padding" "60px 0" ]

contentRowInternal model =
  Grid.row [ Row.centerMd ]
    ( [ (companyName model), (separatorBar model) ]
      ++ (aboutJob model)
      ++ (aboutStudies model)
      ++ (aboutLang model)
      ++ (aboutHobbies model)
    )

companyName model =
  Grid.col [ Col.md12 ] [ div ((companyNameStyle model) ++ (paddingBottom40 model)) [ text (i18n model "my.company.name") ] ]

companyNameStyle model =
  [ style "color" "GhostWhite"
  , style "font-size"
    ( if isDesktop model then "48px"
      else if (isMobile model) then "36px"
      else "24px"
    )
  , style "font-weight" "900"
  , style "text-align" "center"
  ]

separatorBar model =
  Grid.col [ Col.md12 ]
    [ div (separatorBarPadding model)
      [ div (separatorBarStyle model) []
      , div ((separatorBarStyle model) ++ [ style "background-position" "20px" ]) []
      ]
    ]

separatorBarPadding model =
  if isDesktop model then separatorBarPaddingDesktop
  else if (isMobile model) then separatorBarPaddingMobile
  else separatorBarPaddingMobileSmall
separatorBarPaddingMobileSmall = [ style "padding" "0px 30px 30px" ]
separatorBarPaddingMobile = [ style "padding" "0px 50px 40px" ]
separatorBarPaddingDesktop = [ style "padding" "0px 100px 80px" ]

separatorBarStyle model = separatorBarStyleCommon ++
  if isDesktop model then separatorBarStyleDesktop
  else if (isMobile model) then separatorBarStyleMobile
  else separatorBarStyleMobileSmall
separatorBarStyleCommon =
  [ style "background-image" "linear-gradient(to right, GhostWhite 80%, #51D8DD 20%)"
  , style "background-position" "top"
  ]
separatorBarStyleMobileSmall = [ style "background-size" "30px 3px" , style "height" "3px" ]
separatorBarStyleMobile = [ style "background-size" "50px 6.5px" , style "height" "6.5px" ]
separatorBarStyleDesktop = [ style "background-size" "100px 12.5px" , style "height" "12.5px" ]

spacer model = div (spacerStyle model) []
spacerStyle model =
  if isDesktop model then spacerStyleDesktop
  else if (isMobile model) then spacerStyleMobile
  else spacerStyleMobileSmall
spacerStyleMobileSmall = [ style "width" "20px" ]
spacerStyleMobile = [ style "width" "30px" ]
spacerStyleDesktop = [ style "width" "60px" ]

aboutJob model =
  [ Grid.col [ Col.md12 ]
    [ div (aboutMainStyle model)
      [ div (aboutStyle model)
        [ div [] [ text (i18n model "work.company.name") ]
        , div [] [ text (i18n model "work.company.division") ]
        , div [ style "font-weight" "600" ] [ text (i18n model "work.company.role") ]
        ]
      , div ((iconWrapperStyle model) ++ iconWrapperRightStyle)
        [ img [ src "assets/content/shield.svg", width (iconSize model), draggable "False" ] []
        , spacer model
        ]
      ]
    ]
  ]

aboutStudies model =
  [ Grid.col [ Col.md12 ]
    [ div (aboutMainStyle model)
      [ div (iconWrapperStyle model)
        [ spacer model
        , img [ src "assets/content/cap.svg", width (iconSize model), draggable "False" ] []
        ]
      , div ((aboutStyle model) ++ [ style "text-align" "right" ])
        [ div [] [ text (i18n model "studies.master") ]
        , div [] [ text (i18n model "studies.master.university") ]
        , div [] [ text (i18n model "studies.master.spec") ]
        , div [ style "font-weight" "600" ] [ text (i18n model "studies.master.spec.content") ]
        ]
      ]
    ]
  ]

aboutLang model =
  [ Grid.col [ Col.md12 ]
    [ div (aboutMainStyle model)
      [ div (aboutStyle model)
        [ div [] [ text (i18n model "certificate.cae.line.1") ]
        , div [] [ text (i18n model "certificate.cae.line.2") ]
        , span [ style "font-weight" "600" ]  [ text (i18n model "certificate.cae.line.3") ]
        ]
      , div ((iconWrapperStyle model) ++ iconWrapperRightStyle)
          [ img [ src "assets/content/language.svg", width (iconSize model), draggable "False" ] []
          , spacer model
          ]
      ]
    ]
  ]

aboutHobbies model =
  [ Grid.col [ Col.md12 ]
    [ div (aboutMainStyle model)
      [ div (iconWrapperStyle model)
        [ spacer model
        , img [ src "assets/content/tree.svg", width (iconSize model), draggable "False" ] []
        ]
      , div ((aboutStyle model) ++ [ style "text-align" "right" ])
        [ div [ style "font-weight" "600" ] [ text (i18n model "hobby") ]
        , div [] [ text (i18n model "hobby.programming") ]
        , div [] [ text (i18n model "hobby.hiking") ]
        , div [] [ text (i18n model "hobby.travels") ]
        , div [] [ text (i18n model "hobby.fantasy") ]
        ]
      ]
    ]
  ]

aboutMainStyle model = aboutMainStyleCommon ++
  if isDesktop model then aboutMainStyleDesktop
  else if (isMobile model) then aboutMainStyleMobile
  else aboutMainStyleMobileSmall
aboutMainStyleCommon = [ style "width" "100%" , style "display" "flex", style "justify-content" "space-between" ]
aboutMainStyleMobileSmall = [ style "padding" "0 20px" , style "padding-bottom" "40px" ]
aboutMainStyleMobile = [ style "padding" "0 40px" , style "padding-bottom" "60px" ]
aboutMainStyleDesktop = [ style "padding" "0 100px" , style "padding-bottom" "80px" ]

aboutStyle model = aboutStyleCommon ++
  if isDesktop model then aboutStyleDesktop
  else if (isMobile model) then aboutStyleMobile
  else aboutStyleMobileSmall
aboutStyleCommon = [ style "color" "GhostWhite" ]
aboutStyleMobileSmall = [ style "font-size" "14px" ]
aboutStyleMobile = [ style "font-size" "16px" ]
aboutStyleDesktop = [ style "font-size" "36px" ]

iconWrapperStyle model = iconWrapperStyleCommon ++
  if isDesktop model then iconWrapperStyleDesktop
  else if (isMobile model) then iconWrapperStyleMobile
  else iconWrapperStyleMobileSmall
iconWrapperStyleCommon = [ style "display" "flex" ]
iconWrapperStyleMobileSmall = [ style "width" "60px" ]
iconWrapperStyleMobile = [ style "width" "120px" ]
iconWrapperStyleDesktop = [ style "width" "240px" ]

iconWrapperRightStyle = [ style "justifyContent" "flex-end" ]

iconSize model = if (isMobile model) then 60 else 120
