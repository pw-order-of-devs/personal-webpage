module Components.ImagesRow exposing (imagesRow)


import Bootstrap.Grid as Grid
import Bootstrap.Grid.Col as Col
import Bootstrap.Grid.Row as Row

import Html exposing (..)
import Html.Attributes exposing (..)

import Components.Common exposing (..)
import Components.Translation exposing (i18n)


imagesRow model = div (columnStyle ++ (paddedHead model)) [ imagesRowInternal model ]

imagesRowInternal model =
  Grid.row [ Row.centerMd ]
    [ Grid.col [ Col.xs6 ] [ logoWithText model ]
    , Grid.col [ Col.xs6 ] [ myPhoto model ]
    ]

logoWithText model =
  div imagesRowItemStyle
    [ img [ src "assets/me/logo.svg", width (imageWidth model), height (imageHeight model), draggable "False" ] []
    , div (logoTextStyle model) [ text (i18n model "my.name") ]
    ]

logoTextStyle model =
  [ style "position" "absolute"
  , style "font-size" (logoFontSize model)
  , style "font-weight" "600"
  , style "color" "#51D8DD"
  , style "top" "80%"
  , style "left" "44%"
  , style "transform" "rotate(-32deg)"
  ]

myPhoto model =
  div imagesRowItemStyle [ img [ src "assets/me/me.png", width (imageWidth model), height (imageHeight model), draggable "False" ] [] ]

imagesRowItemStyle =
  [ style "width" "100%"
  , style "position" "relative"
  , style "display" "flex"
  , style "justify-content" "center"
  ]

imageWidth model =
  if isDesktop model then 256
  else if (isMobile model) then 128
  else 96

imageHeight model =
  if isDesktop model then 300
  else if (isMobile model) then 150
  else 112

logoFontSize model =
  if isDesktop model then "24px"
  else if (isMobile model) then "12px"
  else "9px"
