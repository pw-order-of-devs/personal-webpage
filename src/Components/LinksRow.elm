module Components.LinksRow exposing (linksRow)


import Bootstrap.Grid as Grid
import Bootstrap.Grid.Col as Col
import Bootstrap.Grid.Row as Row

import Html exposing (..)
import Html.Attributes exposing (..)

import Components.Common exposing (columnStyle, paddedTail)


linkedinLink = "https://www.linkedin.com/in/pawe%C5%82-walus-23121697"
gitlabLink = "https://gitlab.com/pw-order-of-devs"
youtubeLink = "https://www.youtube.com/channel/UCFtPf20Ro_am1UsGPuirAqw"
discordLink = "https://discord.gg/ERCaFYXChQ"

linksRow model = div (columnStyle ++ (paddedTail model))
  [ linksRowInternal "linkedin" linkedinLink "gitlab" gitlabLink
  , linksRowInternal "youtube" youtubeLink "discord" discordLink
  ]

linksRowInternal item1 path1 item2 path2 =
  Grid.row [ Row.centerXs ]
    [ Grid.col [ Col.md3, Col.xs5 ] [ linksRowItem item1 path1 ]
    , Grid.col [ Col.md3, Col.xs5 ] [ linksRowItem item2 path2 ]
    ]

linksRowItem linkName path =
  a [ href path, target "blank" ] [ img [ src (String.concat ["assets/links/", linkName, ".svg"]), draggable "False" ] [] ]
