module Components.Common exposing (..)


import Browser.Dom exposing (Viewport)

import Html.Attributes exposing (style)

import Json.Decode


type alias Model =
  { width : Float
  , height : Float
  , lang : String
  }

initialModel = { width = 0, height = 0, lang = "en" }

type Msg
  = NoOp
  | GotLang String
  | GotViewport Viewport
  | Resize ( Float, Float )

setCurrentDimensions model ( w, h ) =
  { model | width = w, height = h }

type Device
  = MobileSmall
  | Mobile
  | Desktop

checkDeviceType viewport =
  if viewport.width >= 768 then Desktop
  else if viewport.width >= 300 then Mobile
  else MobileSmall

isDesktop viewport = (checkDeviceType viewport) == Desktop
isMobile viewport = (checkDeviceType viewport) == Mobile
isMobileSmall viewport = (checkDeviceType viewport) == MobileSmall


getLang : Json.Decode.Decoder String
getLang = Json.Decode.at [ "view", "navigator", "language" ] Json.Decode.string

decodeLang : String -> String
decodeLang lang =
  let
    default = "en"
    supported = ["en", "pl"]
    parsed =
      case List.head (String.split "_" (String.replace "-" "_" lang)) of
        Just l -> l
        Nothing -> default
  in
    if List.member parsed supported then parsed else default

bodyStyle =
  [ style "-webkit-user-select" "none"
  , style "-khtml-user-select" "none"
  , style "-moz-user-select" "none"
  , style "-o-user-select" "none"
  , style "user-select" "none"
  , style "font-family" "Lato"
  , style "background-color" "LightGray"
  , style "width" "100vw"
  , style "max-width" "100vw"
  , style "height" "100vh"
  , style "overflow-x" "hidden"
  , style "margin" "0 auto"
  , style "padding" "0"
  ]

columnStyle =
  [ style "background-color" "GhostWhite"
  , style "max-width" "1040px"
  , style "margin-right" "auto"
  , style "margin-left" "auto"
  ]

paddingBottom40 model =
  if isDesktop model then [ style "padding-bottom" "40px" ]
  else if isMobile model then [ style "padding-bottom" "30px" ]
  else [ style "padding-bottom" "10px" ]

paddedHead model =
  if isDesktop model then [ style "padding-top" "40px", style "padding-bottom" "75px" ]
  else if isMobile model then [ style "padding-top" "30px",style "padding-bottom" "60px" ]
  else [ style "padding-top" "20px",style "padding-bottom" "40px" ]

paddedTail model =
  if isDesktop model then [ style "padding-bottom" "30px" ]
  else if isMobile model then [ style "padding-bottom" "20px" ]
  else [ style "padding-bottom" "10px" ]

paddedTailSmall model =
  if isDesktop model then [ style "padding-bottom" "10px" ]
  else if isMobile model then [ style "padding-bottom" "5px" ]
  else [ style "padding-bottom" "3px" ]
