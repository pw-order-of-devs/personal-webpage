module Main exposing (main)


import Bootstrap.CDN as CDN
import Bootstrap.Grid as Grid
import Bootstrap.Grid.Col as Col

import Browser
import Browser.Events
import Browser.Dom exposing (Viewport)

import Json.Decode

import Task

import Components.Common exposing (..)
import Components.ContentRow exposing (contentRow)
import Components.ImagesRow exposing (imagesRow)
import Components.LinksRow exposing (linksRow)
import Components.MarksRow exposing (marksRow)


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
  case msg of
    GotLang lang -> ( { model | lang = decodeLang lang } , Cmd.none )
    GotViewport vp -> ( setCurrentDimensions model ( vp.scene.width, vp.scene.height ), Cmd.none )
    Resize ( w, h ) -> ( setCurrentDimensions model ( w, h ), Cmd.none )
    NoOp -> ( model, Cmd.none )

subscriptions _ = Sub.batch
  [ Browser.Events.onResize (\w h -> Resize ( toFloat w, toFloat h ))
  , Browser.Events.onMouseMove (Json.Decode.map GotLang getLang)
  ]

view model =
  Grid.container bodyStyle
    [ CDN.stylesheet
    , Grid.row []
      [ Grid.col [ Col.md12 ]
        [ imagesRow model
        , contentRow model
        , linksRow model
        , marksRow model
      ] ]
    ]

main : Program () Model Msg
main =
  let
    handleResult v =
      case v of
        Err _ -> NoOp
        Ok vp -> GotViewport vp
  in
  Browser.element
    { init = \_ -> ( initialModel, Task.attempt handleResult Browser.Dom.getViewport )
    , view = view
    , subscriptions = subscriptions
    , update = update
    }

